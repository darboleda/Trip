<?php

namespace Api\Transportation;

/**
 *
 */
class TransportFactoryTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test TransportFactory's constructor
     *
     * @return  TransportFactory $train
     * @author Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function testTransportFactory()
    {
        $transportFactory = new TransportFactory();
        $this->assertInstanceOf(TransportFactory::class, $transportFactory);
        return $transportFactory;
    }

    /**
     * Return a standard transport factory
     * @return TransportFactory standar transport factory
     */
    public function trasportFactoryProvider()
    {
        return [
            'tranportFactory' => [new TransportFactory()]
        ];
    }

    /**
     * Test getTransport method
     * @dataProvider trasportFactoryProvider
     * @expectedException  Api\Exceptions\Transportation\TransportFactoryMissingDataException
     * @expectedExceptionCode  Api\Exceptions\ExceptionCodes::TRANSPORT_MISSING_DATA
     *
     * @param  TransportFactory $transportFactory
     * @return void
     */
    public function testGetTransportMissingType(TransportFactory $transportFactory)
    {
        $transport = $transportFactory->getTransport([]);
    }

    /**
     * Test getTransport method
     * @dataProvider trasportFactoryProvider
     * @expectedException  Api\Exceptions\Transportation\TransportFactoryInvalidTransportException
     * @expectedExceptionCode  Api\Exceptions\ExceptionCodes::TRANSPORT_INVALID_TYPE
     *
     * @param  TransportFactory $transportFactory
     * @return void
     */
    public function testGetTransportInvalidType(TransportFactory $transportFactory)
    {
        $transport = $transportFactory->getTransport(
            ['type'=>'ufo', 'name'=> 'one', 'seat' => '1']
        );
    }

    /**
     * Test getTransport method
     * @dataProvider trasportFactoryProvider
     * @expectedException  Api\Exceptions\Transportation\TransportFactoryMissingDataException
     * @expectedExceptionCode  Api\Exceptions\ExceptionCodes::TRANSPORT_MISSING_DATA
     *
     * @param  TransportFactory $transportFactory
     * @return void
     */
    public function testGetTransportMissingName(TransportFactory $transportFactory)
    {
        $transport = $transportFactory->getTransport(['type'=>'bus']);
    }

    /**
     * Test getTransport method
     * @dataProvider trasportFactoryProvider
     *
     * @param  TransportFactory $transportFactory
     * @return void
     */
    public function testGetTransport(TransportFactory $transportFactory)
    {
        $transport = $transportFactory->getTransport(
            ['type'=>'bus', 'name'=> 'one', 'seat' => '1']
        );

        $this->assertInstanceOf(AbstractTransport::class, $transport);
    }
}
