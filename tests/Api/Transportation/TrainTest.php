<?php

namespace Api\Transportation;

/**
 *
 */
class TrainTest extends \PHPUnit_Framework_TestCase
{
    const NAME = 'UH78';
    const SEAT = '57B';

    /**
     * Test Train's constructor
     *
     * @return  Train $train
     * @author Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function testTrain()
    {
        $train = new Train();
        $this->assertInstanceOf(Train::class, $train);
        return $train;
    }

    /**
     * @depends testTrain
     *
     * @return Train $train
     * @author Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function testSetName(Train $train)
    {
        $this->assertInstanceOf(Train::class, $train->setName(self::NAME));
        return $train;
    }

    /**
     * @depends testSetName
     * @author Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function testGetName(Train $train)
    {
        $this->assertEquals(self::NAME, $train->getName());
    }

    /**
     * @depends  testSetName
     * @param  Train  $train
     * @return Train $train
     * @author Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function testSetSeat(Train $train)
    {
        $this->assertInstanceOf(Train::class, $train->setSeat(self::SEAT));
        return $train;
    }

    /**
     * @depends testSetSeat
     * @author Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function testGetSeat(Train $train)
    {
        $this->assertEquals(self::SEAT, $train->getSeat());
    }

    /**
     * @depends testSetSeat
     * @author Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function testGetMessage(Train $train)
    {
        $message = $train->getMessage();
        $this->assertStringStartsWith('Take train', $message);
    }
}
