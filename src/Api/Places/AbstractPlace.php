<?php

namespace Api\Places;

/**
 *
 */
abstract class AbstractPlace implements PlaceInterface
{

    /**
     * Name
     * @var string
     */
    private $name;

    /**
     * ID
     * @var integer
     */
    private $id;

    /**
     * Place's constructor
     *
     * @author Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function __construct($name, $id)
    {
        $this->setName($name);
        $this->setId($id);
    }
}
