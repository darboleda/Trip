<?php

namespace Api\Cards;

use Api\Places\Place;
use Api\Places\AbstractPlace;
use Api\Transportation\AbstractTransport;
use Api\Transportation\Train;
use Api\Transportation\Bus;
use Api\Transportation\Flight;
use Api\Exceptions\Cards\CardFactoryInvalidCardException;
use Api\Transportation\TransportFactoryInterface;
use Api\Places\PlaceFactoryInterface;

/**
 *
 */
interface CardFactoryInterface
{
    /**
     * @param  string            $gate           [description]
     * @param  string            $baggageMessage [description]
     * @param  Place             $origin         [description]
     * @param  Place             $destination    [description]
     * @param  AbstractTransport $transport      [description]
     *
     * @return AbstractCard $card
     *
     * @author  Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function getCard(
        string $gate,
        string $baggageMessage,
        AbstractPlace $origin,
        AbstractPlace $destination,
        AbstractTransport $transport
    ): AbstractCard;

    /**
     *
     * @param  array $card
     * @return AbstractCard $cardInstance
     *
     * @author  Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function buildCard(array $card): AbstractCard;
}
