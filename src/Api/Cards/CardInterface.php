<?php

namespace Api\Cards;

use Api\Places\AbstractPlace;
use Api\Transportation\AbstractTransport;

/**
 *
 */
interface CardInterface
{
    /**
     * Set Card's origin
     *
     * @param AbstractPlace $origin [description]
     * @return  Card $this
     *
     * @author  Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function setOrigin(AbstractPlace $origin);

    /**
     * Get Card's origin
     *
     * @return  AbstractPlace $origin
     *
     * @author  Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function getOrigin(): AbstractPlace;

    /**
     * Set Card's destination
     *
     * @param AbstractPlace $destination [description]
     * @return  Card $this
     *
     * @author  Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function setDestination(AbstractPlace $destination);

    /**
     * Get Card's destination
     *
     * @return  AbstractPlace $origin
     *
     * @author  Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function getDestination(): AbstractPlace;

    /**
     * Set Card's transport
     *
     * @param AbstractTransport $transport [description]
     * @return  Card $this
     *
     * @author  Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function setTransport(AbstractTransport $transport);

    /**
     * Get Card's transport
     *
     * @return  AbstractTransport $transport
     *
     * @author  Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function getTransport(): AbstractTransport;

    /**
     * Set Card's gate
     *
     * @param string $gate [description]
     * @return  Card $this
     *
     * @author  Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function setGate(string $gate);

    /**
     * Get Card's gate
     *
     * @return  string $gate
     *
     * @author  Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function getGate(): string;

    /**
     * Set Card's baggageMessage
     *
     * @param string $baggageMessage [description]
     * @return  Card $this
     *
     * @author  Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function setBaggageMessage(string $baggageMessage);

    /**
     * Get Card's baggageMessage
     *
     * @return  string $baggageMessage
     *
     * @author  Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function getBaggageMessage(): string;

    /**
     * Get detailed itinerary
     * @return string itinerary detailed
     *
     * @author  Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function getItinerary(): string;

    /**
     * Append text to Card's baggageMessage
     *
     * @param string $baggageMessage [description]
     * @return  Card $this
     *
     * @author  Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function appendToBaggageMessage(
        string $baggageMessage = self::WELCOME_MESSAGE
    );
}
