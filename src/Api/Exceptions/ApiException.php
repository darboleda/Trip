<?php

namespace Api\Exceptions;

class ApiException extends \RunTimeException
{
    public function __toString()
    {
        $message = __CLASS__ . ": [{$this->code}]: {$this->message}\n";
        $message .= ' at line: '.$this->getLine.' on file: '.$this->getFile();
    }
}
