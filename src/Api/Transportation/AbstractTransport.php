<?php

namespace Api\Transportation;

use Api\Transportation\TransportationInterface;

abstract class AbstractTransport implements TransportationInterface
{
    /**
     * Name of the transport
     * @var string
     */
    protected $name;

    /**
     * Number of the seat
     * @var string
     */
    protected $seat;
    /**
     * Set the Transportation's name
     *
     * @param string $name name of the trasport (ej. BH7)
     * @return  AbstractTransport $this
     *
     * @author Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function setName(string $name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get Transportation's name
     * @return string $name
     *
     * @author Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the Transportation's seat
     *
     * @param string $seat
     * @return  AbstractTransport $this
     *
     * @author Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function setSeat(string $seat)
    {
        $this->seat = $seat;
        return $this;
    }

    /**
     * Get Transportation's seat
     *
     * @return string $seat
     *
     * @author Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    public function getSeat()
    {
        return $this->seat;
    }

    /**
     * Return part of the message for itinerary
     *
     * @return  string $message
     * @author Diego Arboleda <ing.diego.fernando.arboleda@gmail.com>
     */
    abstract public function getMessage();
}
